package com.rzhyppolite.gmail.mbds.tp02;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ComputeActivity extends AppCompatActivity {

    private TextView textViewNb1, textViewNb2;
    private Button buttonSum, buttonMinus, buttonMultiply,buttomDivide;
    public static final int DIVIDE_BY_ZERO = -5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compute);

        textViewNb1 = findViewById(R.id.textNb1);
        textViewNb2 = findViewById(R.id.textNb2);
        buttonSum = findViewById(R.id.sum);
        buttonMinus = findViewById(R.id.minus);
        buttonMultiply = findViewById(R.id.multiply);
        buttomDivide = findViewById(R.id.divide);
        setUpViews();
        initViews();
    }

    private void setUpViews() {
        // Récupérer l'intention pour appeler la vue
        Intent intent = getIntent();
        // Récuperer la valeur de la cle dans l'intention
        String nb1 = intent.getStringExtra("nb1");
        String nb2 = intent.getStringExtra("nb2");

        textViewNb1.setText(nb1);
        textViewNb2.setText(nb2);

    }



    private void initViews() {

        String nb1 = getIntent().getStringExtra("nb1");
        String nb2 = getIntent().getStringExtra("nb2");


        Intent intent = new Intent();

        buttonSum.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Integer result = Integer.parseInt(String.valueOf(nb1)) + Integer.parseInt(String.valueOf(nb2));
                intent.putExtra("result", result.toString());
                setResult(RESULT_OK, intent);
                finish();

            }
        });


        buttonMinus.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Integer result = Integer.parseInt(String.valueOf(nb1)) - Integer.parseInt(String.valueOf(nb2));
                intent.putExtra("result", result.toString());
                setResult(RESULT_OK, intent);
                finish();

            }
        });


        buttonMultiply.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Integer result = Integer.parseInt(String.valueOf(nb1)) * Integer.parseInt(String.valueOf(nb2));
                intent.putExtra("result", result.toString());
                setResult(RESULT_OK, intent);
                finish();

            }
        });


        buttomDivide.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if(Integer.parseInt(String.valueOf(nb2)) !=0) {
                    Float result = Float.parseFloat(String.valueOf(nb1)) / Float.parseFloat(String.valueOf(nb2));
                    intent.putExtra("result", result.toString());
                    setResult(RESULT_OK, intent);
                }else{
                    setResult(DIVIDE_BY_ZERO, intent);
                }
                finish();
            }
        });



    }
}