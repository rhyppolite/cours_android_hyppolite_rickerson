package com.rzhyppolite.gmail.mbds.tp04;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

public class SecondActivity extends AppCompatActivity {

    private Button button2;
    private static final String TAG = "Statut";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        button2 = findViewById(R.id.btn2);
        fermerActiviter();
        Log.d(TAG, "La methode onCreate est appelee dans SecondActivity");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "La methode onStart est appelee dans SecondActivity ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "La methode onResume est appelee dans SecondActivity ");
    }

    @Override
    protected void onPause() {
        super.onPause();setContentView(R.layout.activity_second);
        Log.d(TAG, "La methode onPause est appelee dans SecondActivity ");

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "La methode onStop est appelee dans SecondActivity ");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "La methode onRestart est appelee dans SecondActivity ");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "La methode onDestroy est appelee dans SecondActivity ");
    }

    private void fermerActiviter(){
        button2.setOnClickListener(v -> {
            finish();
        });
    }
}