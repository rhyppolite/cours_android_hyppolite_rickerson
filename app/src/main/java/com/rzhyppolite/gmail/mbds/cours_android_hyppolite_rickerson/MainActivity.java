package com.rzhyppolite.gmail.mbds.cours_android_hyppolite_rickerson;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button tp01Btn, tp02Btn,tp03Btn,tp04Btn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tp01Btn = findViewById(R.id.btp1);
        tp02Btn = findViewById(R.id.btp2);
        tp03Btn = findViewById(R.id.btp3);
        tp04Btn = findViewById(R.id.btp4);

        initViews();
    }


    private void initViews() {

        tp01Btn.setOnClickListener(view -> {
            Intent intent = new Intent("gestion.mbds.TP01");
            startActivity(intent);
            Toast.makeText(MainActivity.this, "TP01", Toast.LENGTH_SHORT).show();
        });


        tp02Btn.setOnClickListener(v -> {
            Intent intent = new Intent("gestion.mbds.TP02");
            startActivity(intent);
            Toast.makeText(MainActivity.this, "TP02", Toast.LENGTH_SHORT).show();
        });

        tp03Btn.setOnClickListener(v -> {
            Intent intent = new Intent("gestion.mbds.TP03");
            startActivity(intent);
            Toast.makeText(MainActivity.this, "TP03", Toast.LENGTH_SHORT).show();
        });

        tp04Btn.setOnClickListener(v -> {
            Intent intent = new Intent("gestion.mbds.TP04");
            startActivity(intent);
            Toast.makeText(MainActivity.this, "TP04", Toast.LENGTH_SHORT).show();

        });

    }
}