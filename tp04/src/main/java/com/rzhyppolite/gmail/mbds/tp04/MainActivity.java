package com.rzhyppolite.gmail.mbds.tp04;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button button1;
    private static final String TAG = "Statut";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button1 = findViewById(R.id.btn1);
        lancerActiviter();
        Log.d(TAG, "La methode onCreate est appelee dans MainActivity");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "La methode onStart est appelee dans MainActivity ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "La methode onResume est appelee dans MainActivity ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "La methode onPause est appelee dans MainActivity ");

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "La methode onStop est appelee dans MainActivity ");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "La methode onRestart est appelee dans MainActivity ");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "La methode onDestroy est appelee dans MainActivity ");
    }

    private void lancerActiviter(){

        Context context = getBaseContext();
        Intent intent = new Intent(context, SecondActivity.class);

        button1.setOnClickListener(v -> {
            startActivity(intent);

        });
    }
}